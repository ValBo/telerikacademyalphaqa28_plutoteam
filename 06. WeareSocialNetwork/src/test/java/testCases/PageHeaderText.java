package testCases;

public class PageHeaderText {
    public static final String LOGIN_PAGE_HEADER = "Login Page";
    public static final String LATEST_POSTS_HEADER = "Explore all posts";
    public static final String EXPLORE_POST_HEADER = "Explore post";
    public static final String EDIT_POST_HEADER = "Edit post";
    public static final String DELETE_POST_HEADER = "Delete post";
    public static final String POST_SUCCESSFULLY_DELETED_HEADER = "Post deleted successfully";
    public static final String CREATE_NEW_TOPIC_HEADER_TEXT = "Create new post";
    public static final String CREATE_POST_SUCCESSFUL = "Explore all posts";
    public static final String SUCCESSFUL_REGISTRATION_TEXT = "Welcome to our community.";
    public static final String REGISTRATION_PAGE_HEADER = "Join our community";
    public static final String FRIEND_REQUEST_HEADER = "All Requests";
    public static final String N0_FRIEND_REQUESTS = "There are no requests";

}
